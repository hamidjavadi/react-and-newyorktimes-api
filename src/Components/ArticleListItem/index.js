import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import './style.scss';

const ArticleListItem = (props) => {

    const [isLiked, updateIsliked] = useState(false);
    const routeString = props.article.display_title.replace(/ /g, '-');


    const onToggleLike = (event) => {

        event.preventDefault();
        let likedArticles = GetLocalStorage();

        /* Begin add or remove this item to liked list */
        let itemIndex = likedArticles.indexOf(routeString); // I ussed routeString as a unique id
        if (itemIndex !== -1) {
            updateIsliked(false);
            likedArticles.splice(itemIndex, 1);
        } else {
            updateIsliked(true);
            likedArticles.push(routeString);
        }
        /* End add or remove this item to liked list */

        // console.log(likedArticles);
        window.localStorage.setItem('likedArticles', JSON.stringify(likedArticles));

    }

    const GetLocalStorage = () => {

        let likedArticles = window.localStorage.getItem('likedArticles');

        if (likedArticles === null) {
            likedArticles = [];
        } else {
            likedArticles = JSON.parse(likedArticles);
        }

        return likedArticles;

    }

    useEffect(() => {

        let likedArticles = GetLocalStorage();
        let itemIndex = likedArticles.indexOf(routeString); // I ussed routeString as a unique id

        if (itemIndex !== -1) {
            updateIsliked(true);
        } else {
            updateIsliked(false);
        }

    }, [])

    return (
        <div className="article-list-item">
            <NavLink to={`/${routeString}`}>
                <div className="image" style={{ backgroundImage: `url(${props.article.multimedia !== null ? props.article.multimedia.src : ''}` }} >
                    <span className="badge">{props.article.link.type}</span>
                    <div className="show-more-container">
                        <span className="btn show-more">BUY</span>
                    </div>
                </div>
                <div className="content">
                    <h3 className="title">{props.article.display_title}</h3>
                    <p className="meta">Published at {props.article.publication_date}</p>
                    <p className="meta">Watch on Zoom</p>
                    <div className="footer">
                        <span className={isLiked === true ? 'like active' : 'like'} onClick={(event) => onToggleLike(event)}>
                            <i className={isLiked === true ? 'fa fa-heart' : 'fa fa-heart-o'}></i>
                        </span>
                        <span className="price">
                            Free
                        </span>
                    </div>
                </div>

            </NavLink>
        </div>
    );
}

export default ArticleListItem;

import React from 'react';
import './style.scss';

const Loading = () => {

    return (
        <div className="loading">
            <i className="fa fa-spinner fa-spin"></i>
            <span>Loading ...</span>
        </div>
    );
}

export default Loading;

const If = ({ condition, children }) => {
    if (condition) {
        return children;
    } else {
        return '';
    }
}

export default If;

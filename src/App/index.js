import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ArticlesPage from '../Pages/Articles';
import Article from '../Pages/Article';
import NotFoundPage from '../Pages/NotFound';

import './style.scss';


const App = () => {
  return (
    <div className="container">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact>
            <ArticlesPage />
          </Route>
          <Route path="/:routeString" exact>
            <Article />
          </Route>
          <Route>
            <NotFoundPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;

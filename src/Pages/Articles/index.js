import Axios from 'axios';
import ArticleListItem from '../../Components/ArticleListItem';
import ArticlesActions from '../../Store/Articles/actions';
import Config from '../../config';
import If from '../../Components/If';
import Loading from '../../Components/Loading';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss';


const ArticlesPage = (props) => {

    const hasMoreArticles = useSelector(state => {
        return state.ArticleReducer.hasMore;
    });

    const currentPage = useSelector(state => {
        return state.ArticleReducer.currentPage;
    })

    const isLoading = useSelector(state => {
        return state.ArticleReducer.isLoading;
    });

    const articles = useSelector(state => {
        return state.ArticleReducer.articles;
    });

    const scrollHandler = (event) => {

        let windowHeight = window.innerHeight,
            html = document.getElementsByTagName("html")[0],
            scrollHeight = html.scrollHeight,
            scrollTop = html.scrollTop,
            offset = 0;

        if (((scrollTop + windowHeight) >= scrollHeight + offset) && isLoading === false) {

            if (hasMoreArticles === true) {
                fetchNextPageArticles();
            }

        }

    }

    const storeDispatcher = useDispatch();

    const fetchNextPageArticles = () => {

        // Begin set to loading
        storeDispatcher({
            type: ArticlesActions.START_FETCH_ARTICLES
        })
        // End set to loading

        Axios.get(Config.api.url, {
            params: {
                "api-key": Config.api.key,
                offset: currentPage * Config.articlesPerPage,
                order: "by-opening-date"
            }
        })
            .then(response => {

                storeDispatcher({
                    type: ArticlesActions.FETCH_ARTICLES_SUCCESS,
                    payload: {
                        isLoading: false,
                        articles: response.data.results,
                        hasMore: response.data.has_more,
                        currentPage: currentPage + 1
                    }
                })

            }).catch(error => {
                console.log('error', error);
            });

    }

    // Begin Component Did Mount
    useEffect(() => {

        window.addEventListener("scroll", scrollHandler);

        if (articles.length === 0) {
            fetchNextPageArticles();
        }

        // Begin Component Will Unmount
        return () => {
            window.removeEventListener("scroll", scrollHandler);
        }
        // End Component Will Unmount

    }, [articles]);
    // End Component Did Mount

    return (
        <div className="article-list">
            {
                articles.map((article, index) => {
                    return <ArticleListItem key={index} article={article} />
                })
            }
            <If condition={hasMoreArticles}>
                <Loading />
            </If>
        </div>
    );
}


// I used this approach because of Zombie childs
// const mapStateToProps = (state, ownProps) => {
//     return {
//         hasMoreArticles: state.ArticleReducer.hasMore,
//         currentPage: state.ArticleReducer.currentPage,
//         isLoading: state.ArticleReducer.isLoading,
//         articles: state.ArticleReducer.articles,
//     }
// }

// export default connect(mapStateToProps)(ArticlesPage)
export default ArticlesPage

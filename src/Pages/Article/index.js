import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { NavLink, useRouteMatch } from 'react-router-dom';
import './style.scss';

const ArticleListItem = (props) => {

    const [isLiked, updateIsliked] = useState(false);
    const routeMatch = useRouteMatch();
    const routeString = routeMatch.params.routeString;
    const article = useSelector(state => {

        return state.ArticleReducer.articles.find((article) => {
            return article.display_title.replace(/ /g, '-') === routeString;
        })

    });

    const onToggleLike = (event) => {

        event.preventDefault();
        let likedArticles = GetLocalStorage();

        /* Begin add or remove this item to liked list */
        let itemIndex = likedArticles.indexOf(routeString); // I ussed routeString as a unique id
        if (itemIndex !== -1) {
            updateIsliked(false);
            likedArticles.splice(itemIndex, 1);
        } else {
            updateIsliked(true);
            likedArticles.push(routeString);
        }
        /* End add or remove this item to liked list */

        window.localStorage.setItem('likedArticles', JSON.stringify(likedArticles));

    }

    const GetLocalStorage = () => {

        let likedArticles = window.localStorage.getItem('likedArticles');

        if (likedArticles === null) {
            likedArticles = [];
        } else {
            likedArticles = JSON.parse(likedArticles);
        }

        return likedArticles;

    }

    const ArticleMarkup = () => {

        let articleHtml = '';

        if (article !== [] && article !== undefined) {
            articleHtml = <div className="article">
                <div className="image">
                    <img src={article.multimedia !== null && article.multimedia !== undefined ? article.multimedia.src : ''} />
                </div>
                <div className="content">
                    <h1>{article.headline}</h1>
                    <p>{article.summary_short}</p>
                </div>
            </div>
        }


        return articleHtml;

    }

    useEffect(() => {

        let likedArticles = GetLocalStorage();
        let itemIndex = likedArticles.indexOf(routeString); // I ussed routeString as a unique id

        if (itemIndex !== -1) {
            updateIsliked(true);
        } else {
            updateIsliked(false);
        }

    });

    return (
        <div>
            <ArticleMarkup />
            <NavLink to="/" className="btn-back">
                <i className="fa fa-angle-left"></i>
                <span>Back</span>
            </NavLink>
        </div>
    );
}

export default ArticleListItem;

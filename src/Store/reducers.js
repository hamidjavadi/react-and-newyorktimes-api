import { combineReducers } from "redux";
import ArticleReducer from "./Articles";

const StoreReducers = combineReducers({
    ArticleReducer: ArticleReducer,
});

export default StoreReducers;

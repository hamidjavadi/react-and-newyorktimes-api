import Actions from "./actions";

const initialState = {
    isLoading: false,
    articles: [],
    hasMore: false,
    currentPage: 0
};

function ArticleReducer(state = initialState, { type, payload }) {
    switch (type) {
        case Actions.START_FETCH_ARTICLES:

            return Object.assign({}, state, {
                isLoading: true
            })
        case Actions.FETCH_ARTICLES_SUCCESS:

            let stateArticles = [...state.articles];

            payload.articles.forEach(element => {
                stateArticles.push(element);
            });
            payload.articles = stateArticles;

            return {
                ...state,
                ...payload
            }
        case Actions.FETCH_ARTICLES_FAILED:
            return {
                ...state,
                ...payload
            }
        default:
            break;
    }
    return state;
}

export default ArticleReducer;

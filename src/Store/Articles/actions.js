const ArticleActions = {
    START_FETCH_ARTICLES: "START_FETCH_ARTICLES",
    FETCH_ARTICLES_SUCCESS: "FETCH_ARTICLES_SUCCESS",
    FETCH_ARTICLES_FAILED: "FETCH_ARTICLES_FAILED",
    SET_CATEGORY_META: "SET_CATEGORY_META",
}

export default ArticleActions;